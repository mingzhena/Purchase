address 0xCAf7665C9C1Ad87c5DBc5c2b39Fe4cF4 {
module Purchase {
    use 0x1::Option::{Self, Option};
    use 0x1::Token::{Token, Self};
    use 0x1::STC::STC;
    use 0x1::Vector;
    use 0x1::Signer;
    use 0x1::Account;

    const ACTIVE: u8 = 1;
    const LOCKED: u8 = 2;
    const INACTIVE: u8 = 3;

    
    struct Goods<Info: copy + drop + store> has store {
        state: u8,
        info: Info,
        seller: address,
        price: u128,
        s_deposit: Option<Token<STC>>,
        buyer: Option<address>,
        b_deposit: Option<Token<STC>>,
    }

    struct Market<Info: copy + drop + store> has key, store {
        goods_center: vector<Goods<Info>>,
    }

    public fun init<Info: copy + drop + store>(account: &signer) {
	    let addr = Signer::address_of(account);
        if (!exists<Market<Info>>(addr)){
            move_to(account, Market {
                goods_center: Vector::empty<Goods<Info>>()
            });
        };
    }

    fun is_market<Info: copy + drop + store>(market_address: address) : bool {
	    exists<Market<Info>>(market_address)
    }

    public fun publish<Info: copy + drop + store>(account: &signer, market_address: address, s_deposit: Token<STC>, info:Info): u64 acquires Market {
        assert(Self::is_market<Info>(market_address), 20000);
        let deposit = Token::value<STC>(&s_deposit);
        assert(deposit > 1 , 20001);
        let price = deposit / 2;
        let goods = Goods {
            state: ACTIVE,
            info: info,
            seller: Signer::address_of(account),
            price: price,
            s_deposit: Option::some<Token<STC>>(s_deposit),
            buyer: Option::none(),
            b_deposit: Option::none(),
        };
        let market = borrow_global_mut<Market<Info>>(market_address);
        Vector::push_back(&mut market.goods_center, goods);
        Vector::length(&market.goods_center)
    }

    public fun revoke<Info: copy + drop + store>(account: &signer, market_address: address, goods_id: u64) : Token<STC> acquires Market {
        assert(Self::is_market<Info>(market_address), 20000);
        let market = borrow_global_mut<Market<Info>>(market_address);
        let goods_info = Vector::borrow_mut(&mut market.goods_center, goods_id);
        let seller = Signer::address_of(account);
        assert(seller == goods_info.seller, 20002);
        assert(goods_info.state == ACTIVE, 20003);
        goods_info.state = INACTIVE;
        Option::extract(&mut goods_info.s_deposit)
    }

    public fun order<Info: copy + drop + store>(account: &signer, market_address: address, goods_id: u64, b_deposit: Token<STC>) acquires Market {
        assert(Self::is_market<Info>(market_address), 20000);
        let market = borrow_global_mut<Market<Info>>(market_address);
        let goods_info = Vector::borrow_mut(&mut market.goods_center, goods_id);
        let deposit = Token::value<STC>(&b_deposit);
        assert(goods_info.state == ACTIVE, 20004);
        let price2 = goods_info.price * 2;
        assert(price2 <= deposit, 20005);
        let buyer = Signer::address_of(account);
        goods_info.state = LOCKED;
        Option::fill(&mut goods_info.buyer, buyer);
        Option::fill(&mut goods_info.b_deposit, b_deposit);
    }

    public fun done<Info: copy + drop + store>(account: &signer, market_address: address, goods_id: u64):Token<STC>  acquires Market {
        assert(Self::is_market<Info>(market_address), 20000);
        let market = borrow_global_mut<Market<Info>>(market_address);
        let goods_info = Vector::borrow_mut(&mut market.goods_center, goods_id);
        let buyer = Signer::address_of(account);
        assert(&buyer == Option::borrow(&goods_info.buyer), 20006);
        assert(goods_info.state == LOCKED, 20007);
        goods_info.state = INACTIVE;
        let s_deposit = Option::extract(&mut goods_info.s_deposit);
        let b_deposit = Option::extract(&mut goods_info.b_deposit);
        let deposit = Token::join<STC>(s_deposit, b_deposit);
        let (buyer_coins, seller_coins) = Token::split<STC>(deposit, goods_info.price);
        Account::deposit(goods_info.seller, seller_coins);
        buyer_coins
    }
}    
}
